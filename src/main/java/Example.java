import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Arrays;

//This class is used to consume data from JSON
public class Example {
    private String text;
    private Tr[] tr;

    public Example() {
    }

    public Example(String text, Tr[] tr) {
        this.text = text;
        this.tr = tr;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Tr[] getTr() {
        return tr;
    }

    public void setTr(Tr[] tr) {
        this.tr = tr;
    }

    @Override
    public String toString() {
        return "Example{" +
                "text='" + text + '\'' +
                ", trs=" + Arrays.toString(tr) +
                '}';
    }
}
