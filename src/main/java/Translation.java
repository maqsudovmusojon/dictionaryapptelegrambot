//This class is used to consume data from JSON
import java.util.Arrays;

public class Translation {
    private String text;
    private String pos;
    private String asp;
    private Integer fr;
    private Synonym[] syn;
    private Mean[] mean;
    private Example[] ex;

    public Translation() {
    }

    public Translation(String text, String pos, String asp, Integer fr, Synonym[] syn, Mean[] mean, Example[] ex) {
        this.text = text;
        this.pos = pos;
        this.asp = asp;
        this.fr = fr;
        this.syn = syn;
        this.mean = mean;
        this.ex = ex;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getAsp() {
        return asp;
    }

    public void setAsp(String asp) {
        this.asp = asp;
    }

    public Integer getFr() {
        return fr;
    }

    public void setFr(Integer fr) {
        this.fr = fr;
    }

    public Synonym[] getSyn() {
        return syn;
    }

    public void setSyn(Synonym[] syn) {
        this.syn = syn;
    }

    public Mean[] getMean() {
        return mean;
    }

    public void setMean(Mean[] mean) {
        this.mean = mean;
    }

    public Example[] getEx() {
        return ex;
    }

    public void setEx(Example[] ex) {
        this.ex = ex;
    }

    @Override
    public String toString() {
        return "Translation{" +
                "text='" + text + '\'' +
                ", pos='" + pos + '\'' +
                ", asp='" + asp + '\'' +
                ", fr=" + fr +
                ", syn=" + Arrays.toString(syn) +
                ", mean=" + Arrays.toString(mean) +
                ", ex=" + Arrays.toString(ex) +
                '}';
    }


}
