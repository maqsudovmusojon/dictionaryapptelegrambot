import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//This class is used to consume data from JSON
public class Definition {
    private String text;
    private String pos;
    private String ts;
    private String fl;
    private Translation[] tr;

    public Definition() {
    }

    public Definition(String text, String pos, String ts, String fl, Translation[] tr) {
        this.text = text;
        this.pos = pos;
        this.ts = ts;
        this.fl = fl;
        this.tr = tr;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getFl() {
        return fl;
    }

    public void setFl(String fl) {
        this.fl = fl;
    }

    public Translation[] getTr() {
        return tr;
    }

    public void setTr(Translation[] tr) {
        this.tr = tr;
    }

    @Override
    public String toString() {
        return "Definition{" +
                "text='" + text + '\'' +
                ", pos='" + pos + '\'' +
                ", ts='" + ts + '\'' +
                ", fl='" + fl + '\'' +
                ", tr=" + tr +
                '}';
    }


}
