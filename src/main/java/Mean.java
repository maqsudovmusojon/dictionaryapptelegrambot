import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//This class is used to consume data from JSON
public class Mean {
    private String text;

    public Mean() {
    }

    public Mean(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Mean{" +
                "text='" + text + '\'' +
                '}';
    }
}
