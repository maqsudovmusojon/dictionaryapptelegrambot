import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Head {
    @Override
    public String toString() {
        return "Head{}";
    }
}
