//This class is used to consume data from JSON
public class Tr {
    private String text;

    public Tr() {
    }

    public Tr(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Tr{" +
                "text='" + text + '\'' +
                '}';
    }
}
