import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
//This class is used to consume data from JSON
public class Synonym {
    private String text;
    private String pos;
    private String asp;
    private Integer fr;

    public Synonym() {
    }

    public Synonym(String text, String pos, String asp, Integer fr) {
        this.text = text;
        this.pos = pos;
        this.asp = asp;
        this.fr = fr;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getAsp() {
        return asp;
    }

    public void setAsp(String asp) {
        this.asp = asp;
    }

    public Integer getFr() {
        return fr;
    }

    public void setFr(Integer fr) {
        this.fr = fr;
    }

    @Override
    public String toString() {
        return "Synonym{" +
                "text='" + text + '\'' +
                ", pos='" + pos + '\'' +
                ", asp='" + asp + '\'' +
                ", fr=" + fr +
                '}';
    }
}
