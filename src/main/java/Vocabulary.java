//This class is used to consume data from JSON
import java.util.Arrays;
public class Vocabulary {
    private Head head;
    private Definition[] def;

    public Vocabulary() {
    }

    public Vocabulary(Head head, Definition[] def) {
        this.head = head;
        this.def = def;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public Definition[] getDef() {
        return def;
    }

    public void setDef(Definition[] def) {
        this.def = def;
    }

    @Override
    public String toString() {
        return "Vocabulary{" +
                "head=" + head +
                ", def=" + Arrays.toString(def) +
                '}';
    }
}


