import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

//Here we extended our class to TelegramLongPollingBot to override its methods
public class DictionaryApplication extends TelegramLongPollingBot {
    //These variables are to know which language is active
    int counterRU=0;
    int counterEN=0;
    int counterTR=0;

    //Example API: https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=dict.1.1.20210920T170329Z.b8e5d2bc8975c000.fce1fcd48979520483ea98a5cdc109b6a8e0feab&lang=en-ru&text=go

    //Driver method
    public static void main(String[] args) {
        //Here we initialized Api of Telegram
        ApiContextInitializer.init();
        TelegramBotsApi api = new TelegramBotsApi();

        try {
            //Here we assigned our custom class to Telegram API
            api.registerBot(new DictionaryApplication());
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }


    //This is the method of Telegram bot to get info from user on telegram bot
    @Override
    public void onUpdateReceived(Update update) {

        //This is to get data from JSON
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        URL url;
        URL url1;
        Vocabulary vocabulary;
        Vocabulary vocabulary1;


        //We are setting new message and writing its logic
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId());
        String word;
        //This is unique token of Yandex API to get data in JSON format
        final String yandexApiKey = "dict.1.1.20210920T170329Z.b8e5d2bc8975c000.fce1fcd48979520483ea98a5cdc109b6a8e0feab";
        if(update.getMessage().getText().equals("/start")){
            sendMessage.setText("Welcome \"" + update.getMessage().getChat().getFirstName() + "\" to tranlation app\nYou can tranlate any word within 3 languages");
            //In this part we are customizing our telegram bot's interface
            ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
            sendMessage.setReplyMarkup(replyKeyboardMarkup);
            replyKeyboardMarkup.setResizeKeyboard(true);
            replyKeyboardMarkup.setSelective(true);
            replyKeyboardMarkup.setOneTimeKeyboard(true);

            List<KeyboardRow> keyboardRows  = new ArrayList<>();
            KeyboardRow row1 = new KeyboardRow();
            KeyboardButton button1 = new KeyboardButton("RU");
            KeyboardButton button2 = new KeyboardButton("EN");
            KeyboardButton button3 = new KeyboardButton("TR");
            row1.add(button1);
            row1.add(button2);
            row1.add(button3);
            keyboardRows.add(row1);

            replyKeyboardMarkup.setKeyboard(keyboardRows);
            //-------------------------------------------
        }else{
            word = update.getMessage().getText();
            //Here we are checking which language is selecting
            if (word.equals("RU") || word.equals("EN") || word.equals("TR")){
                switch (word){
                    case "RU" ->{
                            sendMessage.setText("You set the language to " + word + ", please enter the word to translate");
                        if (counterRU==0){
                            counterEN =0;
                            counterTR =0;
                            counterRU++;
                        }



                    }
                    case "EN" ->{
                        sendMessage.setText("You set the language to " + word + ", please enter the word to translate");
                        if (counterEN==0){
                            counterRU =0;
                            counterTR =0;
                            counterEN++;
                        }

                    }
                    case"TR"->{
                        sendMessage.setText("You set the language to " + word + ", please enter the word to translate");
                        if (counterTR==0){
                            counterRU =0;
                            counterEN =0;
                            counterTR++;
                        }

                    }
                }
            }
            else {
                if (counterEN==1){
                    try {
                        //Here we are addressing to Yandex Dictionary API to get Language data
                        url = new URL("https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=" + yandexApiKey + "&lang=" + "en-ru" + "&text=" + word);
                        url1 = new URL("https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=" + yandexApiKey + "&lang=" + "en-tr" + "&text=" + word);
                        URLConnection connection  = url.openConnection();
                        URLConnection connection1 = url1.openConnection();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        BufferedReader reader1 = new BufferedReader(new InputStreamReader(connection1.getInputStream()));
                        Type type = new TypeToken<Vocabulary>(){}.getType();
                        Type type1 = new TypeToken<Vocabulary>(){}.getType();
                        vocabulary = gson.fromJson(reader, type);
                        vocabulary1 = gson.fromJson(reader1, type1);

                        sendMessage.setText("Russian: " + vocabulary.getDef()[0].getTr()[0].getText()+"\n" +
                                "Turkish: " + vocabulary1.getDef()[0].getTr()[0].getText());

                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }else if (counterRU ==1){
                    try {
                        //Here we are addressing to Yandex Dictionary API to get Language data
                        url = new URL("https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=" + yandexApiKey + "&lang=" + "ru-en" + "&text=" + word);
                        url1 = new URL("https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=" + yandexApiKey + "&lang=" + "ru-tr" + "&text=" + word);
                        URLConnection connection  = url.openConnection();
                        URLConnection connection1 = url1.openConnection();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        BufferedReader reader1 = new BufferedReader(new InputStreamReader(connection1.getInputStream()));
                        Type type = new TypeToken<Vocabulary>(){}.getType();
                        Type type1 = new TypeToken<Vocabulary>(){}.getType();
                        vocabulary = gson.fromJson(reader, type);
                        vocabulary1 = gson.fromJson(reader1, type1);
                        sendMessage.setText("English: " + vocabulary.getDef()[0].getTr()[0].getText()+"\n" +
                                "Turkish: " + vocabulary1.getDef()[0].getTr()[0].getText());

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }else if (counterTR ==1){
                    try {
                        //Here we are addressing to Yandex Dictionary API to get Language data
                        url = new URL("https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=" + yandexApiKey + "&lang=" + "tr-en" + "&text=" + word);
                        url1 = new URL("https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=" + yandexApiKey + "&lang=" + "tr-ru" + "&text=" + word);
                        URLConnection connection  = url.openConnection();
                        URLConnection connection1 = url1.openConnection();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        BufferedReader reader1 = new BufferedReader(new InputStreamReader(connection1.getInputStream()));
                        Type type = new TypeToken<Vocabulary>(){}.getType();
                        Type type1 = new TypeToken<Vocabulary>(){}.getType();
                        vocabulary = gson.fromJson(reader, type);
                        vocabulary1 = gson.fromJson(reader1, type1);
                        sendMessage.setText("English: " + vocabulary.getDef()[0].getTr()[0].getText()+"\n" +
                                "Russian: " + vocabulary1.getDef()[0].getTr()[0].getText());

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }else
                    sendMessage.setText("Please select language option!!!");
            }


        }


        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }

    //Here we are registering our bot with its unique username and token
    @Override
    public String getBotUsername() {
        return "eng_rus_tur_translateBot";
    }

    @Override
    public String getBotToken() {
        return "2012128318:AAGOFP0EFJm3TDmlsXM_iFnB9l8iaBt0kl8";
    }
}
